#
# Copyright (c) 2021, RISC OS Open Ltd
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of RISC OS Open Ltd nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Makefile for SamplePlayer
#

COMPONENT  = SamplePlayer
ROM_SOURCE = PlayMod.s
RESOURCES  = no

include AAsmModule

# Extra static rules for Replay codecs and ADPCM resources
ifeq ("${RTYPE}","A4")
RBASENAME  = play
REXTRA     = replayres
else
RBASENAME  = Sound${RTYPE}
endif
replay: ${REXTRA} ${DIRS}
	${MKDIR} ${INSTDIR}
	${AS} ${ASFLAGS} -o ${RTYPE}.o   PlayMod.s -pd "RType SETS \"${RTYPE}\""
	${LD} -bin -o ${INSTDIR}${SEP}${RBASENAME}   ${RTYPE}.o
	${AS} ${ASFLAGS} -o ${RTYPE}x2.o PlayMod.s -pd "RType SETS \"${RTYPE}x2\""
	${LD} -bin -o ${INSTDIR}${SEP}${RBASENAME}x2 ${RTYPE}x2.o
replayres:
	${MKDIR} ${INSTDIR}
	${CP} LocalRes:Info ${INSTDIR}${SEP}Info ${CPFLAGS}

# Dynamic dependencies:
